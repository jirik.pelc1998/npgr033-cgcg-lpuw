//From here: https://gist.github.com/phi-lira/225cd7c5e8545be602dca4eb5ed111ba

// When creating shaders for Universal Render Pipeline you can you the ShaderGraph which is super AWESOME!
// However, if you want to author shaders in shading language you can use this teamplate as a base.
// Please note, this shader does not necessarily match perfomance of the built-in URP Lit shader.
// This shader works with URP 7.1.x and above
Shader "Universal Render Pipeline/Custom/Physically Based Example"
{
    Properties
    {
        // Specular vs Metallic workflow
        [HideInInspector] _WorkflowMode("WorkflowMode", Float) = 1.0

        [MainColor] _BaseColor("Color", Color) = (0.5,0.5,0.5,1)
        [MainTexture] _BaseMap("Albedo", 2D) = "white" {}

        _Cutoff("Alpha Cutoff", Range(0.0, 1.0)) = 0.5

        _Smoothness("Smoothness", Range(0.0, 1.0)) = 0.5
        _GlossMapScale("Smoothness Scale", Range(0.0, 1.0)) = 1.0
        _SmoothnessTextureChannel("Smoothness texture channel", Float) = 0

        [Gamma] _Metallic("Metallic", Range(0.0, 1.0)) = 0.0
        _MetallicGlossMap("Metallic", 2D) = "white" {}

        _SpecColor("Specular", Color) = (0.2, 0.2, 0.2)
        _SpecGlossMap("Specular", 2D) = "white" {}

        [ToggleOff] _SpecularHighlights("Specular Highlights", Float) = 1.0
        [ToggleOff] _EnvironmentReflections("Environment Reflections", Float) = 1.0

        _BumpScale("Scale", Float) = 1.0
        _BumpMap("Normal Map", 2D) = "bump" {}

        _OcclusionStrength("Strength", Range(0.0, 1.0)) = 1.0
        _OcclusionMap("Occlusion", 2D) = "white" {}

        _EmissionColor("Color", Color) = (0,0,0)
        _EmissionMap("Emission", 2D) = "white" {}

        // Blending state
        [HideInInspector] _Surface("__surface", Float) = 0.0
        [HideInInspector] _Blend("__blend", Float) = 0.0
        [HideInInspector] _AlphaClip("__clip", Float) = 0.0
        [HideInInspector] _SrcBlend("__src", Float) = 1.0
        [HideInInspector] _DstBlend("__dst", Float) = 0.0
        [HideInInspector] _ZWrite("__zw", Float) = 1.0
        [HideInInspector] _Cull("__cull", Float) = 2.0

        _ReceiveShadows("Receive Shadows", Float) = 1.0

            // Editmode props
            [HideInInspector] _QueueOffset("Queue offset", Float) = 0.0
    }

    SubShader
    {
        // With SRP we introduce a new "RenderPipeline" tag in Subshader. This allows to create shaders
        // that can match multiple render pipelines. If a RenderPipeline tag is not set it will match
        // any render pipeline. In case you want your subshader to only run in LWRP set the tag to
        // "UniversalRenderPipeline"
        Tags{"RenderType" = "Transparent" "RenderPipeline" = "UniversalRenderPipeline" "IgnoreProjector" = "True"}
        LOD 300

        // ------------------------------------------------------------------
        // Forward pass. Shades GI, emission, fog and all lights in a single pass.
        // Compared to Builtin pipeline forward renderer, LWRP forward renderer will
        // render a scene with multiple lights with less drawcalls and less overdraw.
        Pass
        {
            // "Lightmode" tag must be "UniversalForward" or not be defined in order for
            // to render objects.
            Name "StandardLit"
            Tags{"LightMode" = "UniversalForward"}

            Blend[_SrcBlend][_DstBlend]
            ZWrite[_ZWrite]
            Cull[_Cull]

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard SRP library
            // All shaders must be compiled with HLSLcc and currently only gles is not using HLSLcc by default
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 2.0

            // -------------------------------------
            // Material Keywords
            // unused shader_feature variants are stripped from build automatically
            #pragma shader_feature _NORMALMAP
            #pragma shader_feature _ALPHATEST_ON
            #pragma shader_feature _ALPHAPREMULTIPLY_ON
            #pragma shader_feature _EMISSION
            #pragma shader_feature _METALLICSPECGLOSSMAP
            #pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A
            #pragma shader_feature _OCCLUSIONMAP

            #pragma shader_feature _SPECULARHIGHLIGHTS_OFF
            #pragma shader_feature _GLOSSYREFLECTIONS_OFF
            #pragma shader_feature _SPECULAR_SETUP
            #pragma shader_feature _RECEIVE_SHADOWS_OFF

            // -------------------------------------
            // Universal Render Pipeline keywords
            // When doing custom shaders you most often want to copy and past these #pragmas
            // These multi_compile variants are stripped from the build depending on:
            // 1) Settings in the LWRP Asset assigned in the GraphicsSettings at build time
            // e.g If you disable AdditionalLights in the asset then all _ADDITIONA_LIGHTS variants
            // will be stripped from build
            // 2) Invalid combinations are stripped. e.g variants with _MAIN_LIGHT_SHADOWS_CASCADE
            // but not _MAIN_LIGHT_SHADOWS are invalid and therefore stripped.
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            #pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

            //--------------------------------------

            #pragma vertex LitPassVertex
            #pragma fragment LitPassFragment

            // Including the following two function is enought for shading with Universal Pipeline. Everything is included in them.
            // Core.hlsl will include SRP shader library, all constant buffers not related to materials (perobject, percamera, perframe).
            // It also includes matrix/space conversion functions and fog.
            // Lighting.hlsl will include the light functions/data to abstract light constants. You should use GetMainLight and GetLight functions
            // that initialize Light struct. Lighting.hlsl also include GI, Light BDRF functions. It also includes Shadows.

            // Required by all Universal Render Pipeline shaders.
            // It will include Unity built-in shader variables (except the lighting variables)
            // (https://docs.unity3d.com/Manual/SL-UnityShaderVariables.html
            // It will also include many utilitary functions. 
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

            // Include this if you are doing a lit shader. This includes lighting shader variables,
            // lighting and shadow functions
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"

            // Material shader variables are not defined in SRP or LWRP shader library.
            // This means _BaseColor, _BaseMap, _BaseMap_ST, and all variables in the Properties section of a shader
            // must be defined by the shader itself. If you define all those properties in CBUFFER named
            // UnityPerMaterial, SRP can cache the material properties between frames and reduce significantly the cost
            // of each drawcall.
            // In this case, for sinmplicity LitInput.hlsl is included. This contains the CBUFFER for the material
            // properties defined above. As one can see this is not part of the ShaderLibrary, it specific to the
            // LWRP Lit shader.
            #include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"

            struct Attributes
            {
                float4 positionOS   : POSITION;
                float3 normalOS     : NORMAL;
                float4 tangentOS    : TANGENT;
                float2 uv           : TEXCOORD0;
                float2 uvLM         : TEXCOORD1;
            };

            struct Varyings
            {
                float2 uv                       : TEXCOORD0;
                float2 uvLM                     : TEXCOORD1;
                float4 positionWSAndFogFactor   : TEXCOORD2; // xyz: positionWS, w: vertex fog factor
                half3  normalWS                 : TEXCOORD3;

#if _NORMALMAP
                half3 tangentWS                 : TEXCOORD4;
                half3 bitangentWS               : TEXCOORD5;
#endif

#ifdef _MAIN_LIGHT_SHADOWS
                float4 shadowCoord              : TEXCOORD6; // compute shadow coord per-vertex for the main light
#endif
                float4 positionCS               : SV_POSITION;
                float alphaFactor               : TEXCOORD7;
            };

            
            
    //====================================================================================
    // PARAMETERS
    //====================================================================================


            struct Molecule
            {
                float3 position; // 12
                float3 positionPredicted; // 24
                float3 velocity; // 36
                float density; // 40
            };

            uniform float _Transparency;
            uniform float _Scale;
            uniform float4 _TintColor;
            uniform uint _MoleculeCount;
            uniform float _AdjustmentRadius; 
            uniform float _FrontFlattenFactor;
            uniform float _FrontSkewFactor;
            uniform float _BackFlattenFactor;
            uniform float _BackSkewFactor;
            uniform float _NormalDifferentiator;
            uniform float _NoiseDifferentiator;
            uniform float _NoiseScale;
            uniform float _DepthDiff;
            uniform float4x4 _ParentTransform;
            uniform float _AlphaFactorMax;
            uniform float _AlphaFactorInterpolator;
            uniform float _AlphaFactorWindowRadius;

            StructuredBuffer<Molecule> _Molecules;
            
            
            
    //====================================================================================
    // OPTIMIZATION
    //====================================================================================

            
            uniform float3 _boundsSize;
            uniform float _smoothingRadius;
            #define _moleculesCount _MoleculeCount
            #define _molecules _Molecules

            StructuredBuffer<uint> _moleculeIndices;
            StructuredBuffer<uint> _moleculeCellIndices;
            StructuredBuffer<uint> _cellOffsets;


            int3 GetCell(float3 position)
            {
                float3 halfSize = _boundsSize / 2;    
                int x = (position.x + halfSize.x) / _smoothingRadius;
                int y = (position.y + halfSize.y) / _smoothingRadius;
                int z = (position.z + halfSize.z) / _smoothingRadius;
                return int3(x, y, z);
            }

            inline uint HashCell(in int3 cellIndex)
            {
                const uint p1 = 41524877;
                const uint p2 = 54606337;
                const uint p3 = 83494273;
    
                int n = p1 * cellIndex.x ^ p2 * cellIndex.y ^ p3 * cellIndex.z;
                n %= _MoleculeCount;
    
                return n;
            }


#define FOREACH_NEIGHBOR(it, offset, moleculeIndex) \
    for(int3 cellIndex = GetCell(_molecules[moleculeIndex].position), _iterate_once__1=int3(0,0,0); (_iterate_once__1.x = ! _iterate_once__1.x) ; )\
    for (int i = -(offset); i <= (offset); ++i) \
        for (int j = -(offset);j <= (offset); ++j) \
            for (int k = -(offset);k <= (offset); ++k) \
                for(uint _iterate_once__2=0, \
                    hashedNeighbourCellIndex = HashCell(cellIndex + int3(i, j, k)),\
                    neighbourIterator = _cellOffsets[hashedNeighbourCellIndex]; \
                        (_iterate_once__2 = ! _iterate_once__2) ;) \
                for(uint it ; neighbourIterator != 99999999 && neighbourIterator < _moleculesCount && (((it) = _moleculeIndices[neighbourIterator]), 1) && (_moleculeCellIndices[it] != hashedNeighbourCellIndex) ; ++neighbourIterator) \

            
    //====================================================================================
    // GEOMETRY
    //====================================================================================

            float4x4 makeTranslationMatrix(float3 push ){
                float4x4 translation = float4x4(
                    1, 0, 0, push.x,
                    0, 1, 0, push.y,
                    0, 0, 1, push.z,
                    0, 0, 0, 1
                );
                return translation; 
            }
            float4x4 makeScalingMatrix(float3 s ){
                float4x4 m = float4x4(
                    s.x, 0,  0,  0,
                     0, s.y, 0,  0,
                     0,  0, s.z, 0,
                     0,  0,  0,  1
                );
                return m;
            }
            float4x4 makeXRotationMatrix(float a ){
                float4x4 m = float4x4(
                     1,    0,      0,    0,
                     0, cos(a),  sin(a), 0,
                     0, -sin(a), cos(a), 0,
                     0,    0,      0,    1
                );
                return m;
            }
            float4x4 makeYRotationMatrix(float a ){
                float4x4 m = float4x4(
                   cos(a), 0, -sin(a),0,
                     0,    1,   0,    0,
                   sin(a), 0, cos(a), 0,
                     0,    0,   0,    1
                );
                return m;
            }
            float4x4 makeZRotationMatrix(float a){
                float4x4 m = float4x4(
                   cos(a), -sin(a), 0, 0,
                   sin(a),  cos(a), 0, 0,
                     0,      0,     1, 0,
                     0,      0,     0, 1
                );
                return m;
            }
            float3 hsv_to_rgb(float3 HSV)
            {
                    float3 RGB = HSV.z;
           
                    float var_h = HSV.x * 6;
                    float var_i = floor(var_h);   // Or ... var_i = floor( var_h )
                    float var_1 = HSV.z * (1.0 - HSV.y);
                    float var_2 = HSV.z * (1.0 - HSV.y * (var_h-var_i));
                    float var_3 = HSV.z * (1.0 - HSV.y * (1-(var_h-var_i)));
                    if      (var_i == 0) { RGB = float3(HSV.z, var_3, var_1); }
                    else if (var_i == 1) { RGB = float3(var_2, HSV.z, var_1); }
                    else if (var_i == 2) { RGB = float3(var_1, HSV.z, var_3); }
                    else if (var_i == 3) { RGB = float3(var_1, var_2, HSV.z); }
                    else if (var_i == 4) { RGB = float3(var_3, var_1, HSV.z); }
                    else                 { RGB = float3(HSV.z, var_1, var_2); }
           
               return (RGB);
            }
            
            //copypasted from official Unity docs (https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Gradient-Noise-Node.html)
            //BEGINCOPYPASTA
            float2 unity_gradientNoise_dir(float2 p)
            {
                p = p % 289;
                float x = (34 * p.x + 1) * p.x % 289 + p.y;
                x = (34 * x + 1) * x % 289;
                x = frac(x / 41) * 2 - 1;
                return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
            }

            float unity_gradientNoise(float2 p)
            {
                float2 ip = floor(p);
                float2 fp = frac(p);
                float d00 = dot(unity_gradientNoise_dir(ip), fp);
                float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
                float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
                float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
            }

            float Unity_GradientNoise_float(float2 UV, float Scale)
            {
                return unity_gradientNoise(UV * Scale) + 0.5;
            }
            //ENDCOPYPASTA


            float distanceSqr(float3 a, float3 b){
                float3 diff = b - a;
                return dot(diff, diff);
            }

            float smoothingKernel(float distanceSqr, float maxDistanceSqr)
            {
                if(distanceSqr >= maxDistanceSqr)
                    return 0;
                
                float ret = maxDistanceSqr - distanceSqr;
                return ret *ret;
            }

            float3 flat_shading_compute_normal(float3 position_in_object_space){
                float3 pos = position_in_object_space;
                float3 r = cross(ddy(pos), ddx(pos));
                r = normalize(r);

                //float3x3 tangentTransform_World = float3x3(IN.WorldSpaceTangent, IN.WorldSpaceBiTangent, IN.WorldSpaceNormal);
                //float3 r = TransformWorldToTangent(TransformObjectToWorld(r), tangentTransform_World);

                return r;
            }

#define BASIC_DIRECTIONS_COUNT 6
            float3 BASIC_DIRECTIONS[BASIC_DIRECTIONS_COUNT] = {
                float3(1, 0, 0),
                float3(-1, 0, 0),
                float3(0, 1, 0),
                float3(0, -1, 0),
                float3(0, 0, 1),
                float3(0, 0, -1),
            };


            float compute_alpha_factor(float3 pos, uint moleculeIndx){
                return 0;
                float angles[BASIC_DIRECTIONS_COUNT] = {0,0,0,0,0,0};
                
                float radius2 = _AlphaFactorWindowRadius*_AlphaFactorWindowRadius;

                for(uint i = 0;i < _MoleculeCount; ++i){
                    //if(i == moleculeIndx) continue;
                    Molecule m = _Molecules[i];
                    float dist2 = distanceSqr(m.position, pos);
                    if(dist2 > radius2) continue;

                    float3 posDifference = normalize(m.position - pos);

                    for(uint dir = 0;dir < BASIC_DIRECTIONS_COUNT; ++dir){
                        angles[dir] = max(angles[dir], dot(posDifference, BASIC_DIRECTIONS[dir]));
                    }
                }


                float minDir = angles[0];
                for(uint dir=1;dir<BASIC_DIRECTIONS_COUNT;++dir)
                    minDir = min(minDir, angles[dir]);
                minDir = max(0, minDir);

                return minDir;
            }

            float3 adjust_position_by_neighbors(float3 pos, uint moleculeIndx){
                
                float weightSum = 1;
                float3 ret = pos;


                for(uint i = 0;i < _MoleculeCount; ++i){
                    if(i == moleculeIndx) continue;
                    Molecule m = _Molecules[i];

                    float dist2 = distanceSqr(m.position, pos);
                    float weight = smoothingKernel(dist2, _AdjustmentRadius*_AdjustmentRadius);
                    ret += m.position * weight;
                    weightSum += weight;
                }
                return ret / weightSum;

                //uint moleculeIdx = _moleculeIndices[moleculeIndex];
                //
                //FOREACH_NEIGHBOR(it, 6, moleculeIndx){
                //    //if(i == moleculeIdx) continue;
                //    Molecule m = _Molecules[i];
                //    //float posDifference = m.position - pos;
                //    float dist2 = distanceSqr(m.position, pos);
                //    float weight = smoothingKernel(dist2, _AdjustmentRadius*_AdjustmentRadius);
                //    ret += m.position * weight;
                //    weightSum += weight;
                //}
                
                //{
                //    if (moleculeIndx >= _moleculesCount)
                //        return pos;
                //
                //    uint moleculeIndex = _moleculeIndices[moleculeIndx];
                //    int3 cellIndex = GetCell(_molecules[moleculeIndex].position);
                //
                //    float3 sumPressure = 0;
                //    float3 samplePoint = _molecules[moleculeIndex].positionPredicted;
                //
                //    for (int i = -2; i <= 2; ++i)
                //    {
                //        for (int j = -2; j <= 2; ++j)
                //        {
                //            for (int k = -2; k <= 2; ++k)
                //            {
                //                int3 neighbourCellIndex = cellIndex + int3(i, j, k);
                //                uint hashedNeighbourCellIndex = HashCell(neighbourCellIndex);
                //                uint neighbourIterator = _cellOffsets[hashedNeighbourCellIndex];
                //
                //                while (neighbourIterator != 99999999 && neighbourIterator < _moleculesCount)
                //                {
                //                    uint neighbourMoleculeIndex = _moleculeIndices[neighbourIterator];
                //    
                //                    if (_moleculeCellIndices[neighbourMoleculeIndex] != hashedNeighbourCellIndex)
                //                    {
                //                        break;
                //                    }
                //    
                //                    if (moleculeIndex == neighbourMoleculeIndex)
                //                    {
                //                        neighbourIterator++;
                //                        continue;
                //                    }
                //                    
                //                    Molecule m = _Molecules[neighbourMoleculeIndex];
                //                    //float posDifference = m.position - pos;
                //                    float dist2 = distanceSqr(m.position, pos);
                //                    float weight = smoothingKernel(dist2, _AdjustmentRadius*_AdjustmentRadius);
                //                    ret += m.position * weight;
                //                    weightSum += weight;
                //    
                //                    neighbourIterator++;
                //                }
                //
                //            }
                //
                //        }
                //    }
                //}
            }

            float3 adjust_position_by_velocity(float3 pos, int id){
                float3 ret = pos;
                Molecule m = _Molecules[id];

                float3 offsetFromCenter = pos - m.position;
                float directionFactor = dot(offsetFromCenter, m.velocity);
                
                
                if(directionFactor > 0){
                    ret += offsetFromCenter *directionFactor * _FrontFlattenFactor + m.velocity * directionFactor * _FrontSkewFactor;
                }else{
                    ret += offsetFromCenter *directionFactor * _BackFlattenFactor + m.velocity * directionFactor * _BackSkewFactor;
                }


                return ret;
            }

            float compute_directional_alpha_multiplier(float alphaFactor){
                float ret = clamp(_AlphaFactorMax - alphaFactor, 0, 1)/_AlphaFactorMax;
                return ret * _AlphaFactorInterpolator;
            }


            Varyings LitPassVertex(Attributes input, uint instanceIdx: SV_InstanceID)
            {
                Varyings output;
             
                uint moleculeIdx = instanceIdx;//_moleculeIndices[instanceIdx];

                input.positionOS = mul(mul(makeTranslationMatrix(_Molecules[moleculeIdx].position), makeScalingMatrix(float3(_Scale,_Scale,_Scale))), input.positionOS);

                
                //input.positionOS = float4(adjust_position_by_velocity(input.positionOS.xyz/input.positionOS.w, moleculeIdx), 1);
                //float alphaFactor;
                input.positionOS = float4(adjust_position_by_neighbors(input.positionOS.xyz/input.positionOS.w, moleculeIdx), 1); 

                output.alphaFactor = compute_alpha_factor(input.positionOS.xyz/input.positionOS.w, moleculeIdx);

                input.positionOS = mul(_ParentTransform, input.positionOS);

                //float3 objectSpacePos = input.positionOS.xyz/input.positionOS.w - _Molecules[instanceID].position;
                //input.normalOS = float4(normalize(cross(ddx(objectSpacePos.x), ddy(objectSpacePos.y))), 1); 

                //input.normalOS = float4(adjust_position_by_velocity(input.normalOS, instanceID), 1);
                //input.normalOS = float4(adjust_position_by_neighbors(input.normalOS, instanceID), 1); 

                // VertexPositionInputs contains position in multiple spaces (world, view, homogeneous clip space)
                // Our compiler will strip all unused references (say you don't use view space).
                // Therefore there is more flexibility at no additional cost with this struct.
                VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);

                // Similar to VertexPositionInputs, VertexNormalInputs will contain normal, tangent and bitangent
                // in world space. If not used it will be stripped.
                VertexNormalInputs vertexNormalInput = GetVertexNormalInputs(input.normalOS, input.tangentOS);

                // Computes fog factor per-vertex.
                float fogFactor = ComputeFogFactor(vertexInput.positionCS.z);

                // TRANSFORM_TEX is the same as the old shader library.
                output.uv = TRANSFORM_TEX(input.uv, _BaseMap);
                output.uvLM = input.uvLM.xy * unity_LightmapST.xy + unity_LightmapST.zw;

                output.positionWSAndFogFactor = float4(vertexInput.positionWS, fogFactor);
                output.normalWS = vertexNormalInput.normalWS;

                // Here comes the flexibility of the input structs.
                // In the variants that don't have normal map defined
                // tangentWS and bitangentWS will not be referenced and
                // GetVertexNormalInputs is only converting normal
                // from object to world space
#ifdef _NORMALMAP
                output.tangentWS = vertexNormalInput.tangentWS;
                output.bitangentWS = vertexNormalInput.bitangentWS;
#endif

#ifdef _MAIN_LIGHT_SHADOWS
                // shadow coord for the main light is computed in vertex.
                // If cascades are enabled, LWRP will resolve shadows in screen space
                // and this coord will be the uv coord of the screen space shadow texture.
                // Otherwise LWRP will resolve shadows in light space (no depth pre-pass and shadow collect pass)
                // In this case shadowCoord will be the position in light space.
                output.shadowCoord = GetShadowCoord(vertexInput);
#endif
                // We just use the homogeneous clip position from the vertex input
                output.positionCS = vertexInput.positionCS;
                return output;
            }

            half4 LitPassFragment(Varyings input) : SV_Target
            {
                // Surface data contains albedo, metallic, specular, smoothness, occlusion, emission and alpha
                // InitializeStandarLitSurfaceData initializes based on the rules for standard shader.
                // You can write your own function to initialize the surface data of your shader.
                SurfaceData surfaceData;
                InitializeStandardLitSurfaceData(input.uv, surfaceData);

#if _NORMALMAP
                half3 normalWS = TransformTangentToWorld(surfaceData.normalTS,
                    half3x3(input.tangentWS, input.bitangentWS, input.normalWS));
#else
                half3 normalWS = input.normalWS;
#endif
                normalWS = normalize(normalWS);

#ifdef LIGHTMAP_ON
                // Normal is required in case Directional lightmaps are baked
                half3 bakedGI = SampleLightmap(input.uvLM, normalWS);
#else
                // Samples SH fully per-pixel. SampleSHVertex and SampleSHPixel functions
                // are also defined in case you want to sample some terms per-vertex.
                half3 bakedGI = SampleSH(normalWS);
#endif

                float3 positionWS = input.positionWSAndFogFactor.xyz;
                half3 viewDirectionWS = SafeNormalize(GetCameraPositionWS() - positionWS);

                // BRDFData holds energy conserving diffuse and specular material reflections and its roughness.
                // It's easy to plugin your own shading fuction. You just need replace LightingPhysicallyBased function
                // below with your own.
                BRDFData brdfData;
                InitializeBRDFData(surfaceData.albedo, surfaceData.metallic, surfaceData.specular, surfaceData.smoothness, surfaceData.alpha, brdfData);

                // Light struct is provide by LWRP to abstract light shader variables.
                // It contains light direction, color, distanceAttenuation and shadowAttenuation.
                // LWRP take different shading approaches depending on light and platform.
                // You should never reference light shader variables in your shader, instead use the GetLight
                // funcitons to fill this Light struct.
#ifdef _MAIN_LIGHT_SHADOWS
                // Main light is the brightest directional light.
                // It is shaded outside the light loop and it has a specific set of variables and shading path
                // so we can be as fast as possible in the case when there's only a single directional light
                // You can pass optionally a shadowCoord (computed per-vertex). If so, shadowAttenuation will be
                // computed.
                Light mainLight = GetMainLight(input.shadowCoord);
#else
                Light mainLight = GetMainLight();
#endif

                // Mix diffuse GI with environment reflections.
                half3 color = GlobalIllumination(brdfData, bakedGI, surfaceData.occlusion, normalWS, viewDirectionWS);

                // LightingPhysicallyBased computes direct light contribution.
                color += LightingPhysicallyBased(brdfData, mainLight, normalWS, viewDirectionWS);

                // Additional lights loop
#ifdef _ADDITIONAL_LIGHTS

                // Returns the amount of lights affecting the object being renderer.
                // These lights are culled per-object in the forward renderer
                int additionalLightsCount = GetAdditionalLightsCount();
                for (int i = 0; i < additionalLightsCount; ++i)
                {
                    // Similar to GetMainLight, but it takes a for-loop index. This figures out the
                    // per-object light index and samples the light buffer accordingly to initialized the
                    // Light struct. If _ADDITIONAL_LIGHT_SHADOWS is defined it will also compute shadows.
                    Light light = GetAdditionalLight(i, positionWS);

                    // Same functions used to shade the main light.
                    color += LightingPhysicallyBased(brdfData, light, normalWS, viewDirectionWS);
                }
#endif
                // Emission
                color += surfaceData.emission;

                float fogFactor = input.positionWSAndFogFactor.w;

                // Mix the pixel color with fogColor. You can optionaly use MixFogColor to override the fogColor
                // with a custom one.
                color = MixFog(color, fogFactor);
                return half4(color, surfaceData.alpha * _Transparency);
            }
            ENDHLSL
        }

        // Used for rendering shadowmaps
        UsePass "Universal Render Pipeline/Lit/ShadowCaster"

        // Used for depth prepass
        // If shadows cascade are enabled we need to perform a depth prepass. 
        // We also need to use a depth prepass in some cases camera require depth texture
        // (e.g, MSAA is enabled and we can't resolve with Texture2DMS
        UsePass "Universal Render Pipeline/Lit/DepthOnly"

        // Used for Baking GI. This pass is stripped from build.
        UsePass "Universal Render Pipeline/Lit/Meta"
    }

    // Uses a custom shader GUI to display settings. Re-use the same from Lit shader as they have the
    // same properties.
    CustomEditor "UnityEditor.Rendering.Universal.ShaderGUI.LitShader"
}