Shader "Unlit/WaterInstanceShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;

            };
            
            struct Molecule
            {
                float3 position; // 12
                float3 positionPredicted; // 24
                float3 velocity; // 36
                float density; // 40
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            StructuredBuffer<Molecule> _Molecules;


            float4x4 makeTranslationMatrix(float3 push ){
                float4x4 translation = float4x4(
                    1, 0, 0, push.x,
                    0, 1, 0, push.y,
                    0, 0, 1, push.z,
                    0, 0, 0, 1
                );
                return translation;
            }

            v2f vert (appdata v, uint instanceID: SV_InstanceID) 
            {
                v2f o;


                float4 pos = mul(makeTranslationMatrix(_Molecules[instanceID].position), v.vertex);
                o.vertex = UnityObjectToClipPos(pos);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv);
                return col;
            }
            ENDCG
        }
    }
}
