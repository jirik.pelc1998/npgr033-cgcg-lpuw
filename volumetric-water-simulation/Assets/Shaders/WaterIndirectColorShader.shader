﻿#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced '_WorldSpaceCameraPos.w' with '1.0'

#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced '_WorldSpaceCameraPos.w' with '1.0'

Shader "Custom/InstancedIndirectColor" {
    Properties {
        _MoleculeCount("_MoleculeCount", Integer) = 1
	    _Transparency ("_Transparency", float) = 0.5
	    _AdjustmentRadius ("_AdjustmentRadius", float) = 1
	    _Scale ("_Scale", float) = 0.5

	    _FrontFlattenFactor ("_FrontFlattenFactor", float) = 1
	    _FrontSkewFactor ("_FrontSkewFactor", float) = 1
	    _BackFlattenFactor ("_BackFlattenFactor", float) = 1
	    _BackSkewFactor ("_BackSkewFactor", float) = 1
        _NormalDifferentiator("_NormalDifferentiator", float) = 1
        _NoiseDifferentiator("_NoiseDifferentiator", float) = 1
        _NoiseScale("_NoiseScale", float) = 1
        _DepthDiff("_DepthDiff", float) = 1

        _TintColor("_TintColor", Color) = (1,1,1,1)


        
        [HideInInspector]  _QueueOffset ("_QueueOffset", Float) = 0.000000
        [HideInInspector]  _QueueControl ("_QueueControl", Float) = -1.000000
        [HideInInspector] [NoScaleOffset]  unity_Lightmaps ("unity_Lightmaps", 2DArray) = "" { }
        [HideInInspector] [NoScaleOffset]  unity_LightmapsInd ("unity_LightmapsInd", 2DArray) = "" { }
        [HideInInspector] [NoScaleOffset]  unity_ShadowMasks ("unity_ShadowMasks", 2DArray) = "" { }
    }
    SubShader {
        //Tags { "RenderType" = "Opaque" }
        Tags { "QUEUE"="Transparent" "RenderType"="Transparent" "DisableBatching"="False" "RenderPipeline"="UniversalPipeline" "UniversalMaterialType"="Lit" "ShaderGraphShader"="true" "ShaderGraphTargetId"="UniversalLitSubTarget" }
        ZWrite Off
        Cull Off
        Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha


        //Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        //ZWrite Off
        //Blend SrcAlpha OneMinusSrcAlpha
        //Cull front 
        //LOD 100

        Pass {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            
            #include "UnityCG.cginc"
            
            struct appdata_t {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float3 normal : NORMAL;
            };

            struct v2f {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                float3 normal : TEXCOORD1;
                uint instanceID: SV_InstanceID;
            }; 

            
            struct Molecule
            {
                float3 position; // 12
                float3 positionPredicted; // 24
                float3 velocity; // 36
                float density; // 40
            };

            uniform float _Transparency;
            uniform float _Scale;
            uniform float4 _TintColor;
            uniform int _MoleculeCount;
            uniform float _AdjustmentRadius;
            uniform float _FrontFlattenFactor;
            uniform float _FrontSkewFactor;
            uniform float _BackFlattenFactor;
            uniform float _BackSkewFactor;
            uniform float _NormalDifferentiator;
            uniform float _NoiseDifferentiator;
            uniform float _NoiseScale;
            uniform float _DepthDiff;
            uniform float4x4 _ParentTransform;

            StructuredBuffer<Molecule> _Molecules;

            
            float4x4 makeTranslationMatrix(float3 push ){
                float4x4 translation = float4x4(
                    1, 0, 0, push.x,
                    0, 1, 0, push.y,
                    0, 0, 1, push.z,
                    0, 0, 0, 1
                );
                return translation; 
            }
            float4x4 makeScalingMatrix(float3 s ){
                float4x4 m = float4x4(
                    s.x, 0,  0,  0,
                     0, s.y, 0,  0,
                     0,  0, s.z, 0,
                     0,  0,  0,  1
                );
                return m;
            }
            float4x4 makeXRotationMatrix(float a ){
                float4x4 m = float4x4(
                     1,    0,      0,    0,
                     0, cos(a),  sin(a), 0,
                     0, -sin(a), cos(a), 0,
                     0,    0,      0,    1
                );
                return m;
            }
            float4x4 makeYRotationMatrix(float a ){
                float4x4 m = float4x4(
                   cos(a), 0, -sin(a),0,
                     0,    1,   0,    0,
                   sin(a), 0, cos(a), 0,
                     0,    0,   0,    1
                );
                return m;
            }
            float4x4 makeZRotationMatrix(float a){
                float4x4 m = float4x4(
                   cos(a), -sin(a), 0, 0,
                   sin(a),  cos(a), 0, 0,
                     0,      0,     1, 0,
                     0,      0,     0, 1
                );
                return m;
            }
            float3 hsv_to_rgb(float3 HSV)
            {
                    float3 RGB = HSV.z;
           
                    float var_h = HSV.x * 6;
                    float var_i = floor(var_h);   // Or ... var_i = floor( var_h )
                    float var_1 = HSV.z * (1.0 - HSV.y);
                    float var_2 = HSV.z * (1.0 - HSV.y * (var_h-var_i));
                    float var_3 = HSV.z * (1.0 - HSV.y * (1-(var_h-var_i)));
                    if      (var_i == 0) { RGB = float3(HSV.z, var_3, var_1); }
                    else if (var_i == 1) { RGB = float3(var_2, HSV.z, var_1); }
                    else if (var_i == 2) { RGB = float3(var_1, HSV.z, var_3); }
                    else if (var_i == 3) { RGB = float3(var_1, var_2, HSV.z); }
                    else if (var_i == 4) { RGB = float3(var_3, var_1, HSV.z); }
                    else                 { RGB = float3(HSV.z, var_1, var_2); }
           
               return (RGB);
            }
            //copypasted from official Unity docs (https://docs.unity3d.com/Packages/com.unity.shadergraph@6.9/manual/Gradient-Noise-Node.html)
            //BEGINCOPYPASTA
            float2 unity_gradientNoise_dir(float2 p)
            {
                p = p % 289;
                float x = (34 * p.x + 1) * p.x % 289 + p.y;
                x = (34 * x + 1) * x % 289;
                x = frac(x / 41) * 2 - 1;
                return normalize(float2(x - floor(x + 0.5), abs(x) - 0.5));
            }

            float unity_gradientNoise(float2 p)
            {
                float2 ip = floor(p);
                float2 fp = frac(p);
                float d00 = dot(unity_gradientNoise_dir(ip), fp);
                float d01 = dot(unity_gradientNoise_dir(ip + float2(0, 1)), fp - float2(0, 1));
                float d10 = dot(unity_gradientNoise_dir(ip + float2(1, 0)), fp - float2(1, 0));
                float d11 = dot(unity_gradientNoise_dir(ip + float2(1, 1)), fp - float2(1, 1));
                fp = fp * fp * fp * (fp * (fp * 6 - 15) + 10);
                return lerp(lerp(d00, d01, fp.y), lerp(d10, d11, fp.y), fp.x);
            }

            float Unity_GradientNoise_float(float2 UV, float Scale)
            {
                return unity_gradientNoise(UV * Scale) + 0.5;
            }
            //ENDCOPYPASTA


            float distanceSqr(float3 a, float3 b){
                float3 diff = b - a;
                return dot(diff, diff);
            }

            float smoothingKernel(float distanceSqr, float maxDistanceSqr)
            {
                if(distanceSqr >= maxDistanceSqr)
                    return 0;
                
                return maxDistanceSqr - distanceSqr;
            }

            float3 flat_shading_compute_normal(float3 position_in_object_space){
                float3 pos = position_in_object_space;
                float3 r = cross(ddy(pos), ddx(pos));
                r = normalize(r);

                //float3x3 tangentTransform_World = float3x3(IN.WorldSpaceTangent, IN.WorldSpaceBiTangent, IN.WorldSpaceNormal);
                //float3 r = TransformWorldToTangent(TransformObjectToWorld(r), tangentTransform_World);

                return r;
            }


            uint get_closest_molecule(float3 pos){
                uint minMolecule =  0;
                float minDistSqr = 99999;
                for(uint i = 1;i < _MoleculeCount; ++i){
                    float moleculePos = _Molecules[i].position;
                    float distSqr = distanceSqr(pos, moleculePos);
                    if(distSqr < minDistSqr){
                        minDistSqr = distSqr;
                        minMolecule = i;
                    }
                }
                return minMolecule;
            }

            float3 adjust_position_by_neighbors(float3 pos, int ignore){
                float weightSum = 1;
                float3 ret = pos;
                for(uint i = 0;i < _MoleculeCount; ++i){
                    if(i == ignore) continue;
                    Molecule m = _Molecules[i];
                    //float posDifference = m.position - pos;
                    float dist2 = distanceSqr(m.position, pos);
                    float weight = smoothingKernel(dist2, _AdjustmentRadius*_AdjustmentRadius);
                    ret += m.position * weight;
                    weightSum += weight;
                }
                return ret / weightSum;
            }

            float3 adjust_position_by_velocity(float3 pos, int id){
                float3 ret = pos;
                Molecule m = _Molecules[id];

                float3 offsetFromCenter = pos - m.position;
                float directionFactor = dot(offsetFromCenter, m.velocity);
                
                
                if(directionFactor > 0){
                    ret += offsetFromCenter *directionFactor * _FrontFlattenFactor + m.velocity * directionFactor * _FrontSkewFactor;
                }else{
                    ret += offsetFromCenter *directionFactor * _BackFlattenFactor + m.velocity * directionFactor * _BackSkewFactor;
                }


                return ret;
            }


            v2f vert(appdata_t i, uint instanceID: SV_InstanceID) {
                v2f o;

                float4 pos = mul(mul(makeTranslationMatrix(_Molecules[instanceID].position), makeScalingMatrix(float3(_Scale,_Scale,_Scale))), i.vertex);
                //uint closest_molecule = get_closest_molecule(pos.xyz/pos.w);
                //pos = float4(_Molecules[closest_molecule].position, 1);
                pos = float4(adjust_position_by_velocity(pos.xyz/pos.w, instanceID), 1);
                pos = float4(adjust_position_by_neighbors(pos.xyz/pos.w, instanceID), 1); 

                pos = mul(_ParentTransform, pos);


                float4 normal = float4(i.normal,1);
                normal = float4(adjust_position_by_velocity(normal.xyz/normal.w, instanceID), 1);
                normal = float4(adjust_position_by_neighbors(normal.xyz/normal.w, instanceID), 1); 


                //float4 pos = i.vertex + float4(_Molecules[instanceID].position, 0);//mul(_Properties[instanceID].mat, i.vertex);
                o.vertex = UnityObjectToClipPos(pos);
                float density = _Molecules[instanceID].density;
                float densityFactor = 1-clamp((density-0.13)*4, 0, 1); 
                //float4 blueColor = hsv_to_rgb(float3(0.65,1, 1));
                o.color = fixed4(_TintColor.rgb, _Transparency);
                //o.color = fixed4(_TintColor.rgb, _Transparency*densityFactor);
                //o.color = fixed4( hsv_to_rgb(float3(density*2,1, 1)).xyz, _Transparency);
                o.normal = i.normal.xyz;
                o.instanceID = instanceID;

                return o;
            }
            
            fixed4 frag(v2f i) : SV_Target {
                float depth = distanceSqr(_Molecules[i.instanceID].position, _WorldSpaceCameraPos.xyz/1.0);
                fixed4 ret = i.color + float4(i.normal, 0) * _NormalDifferentiator ;//* (depth*_DepthDiff*0.001); 
                //ret.a += Unity_GradientNoise_float(i.vertex.xy, _NoiseScale) * _NoiseDifferentiator;
                
                return ret; 
                //float density = _Molecules[instanceID].density;
                //return fixed4(i.color.rgb, _Transparency*(1-density));
            }
            
            ENDCG
        }
    }
}