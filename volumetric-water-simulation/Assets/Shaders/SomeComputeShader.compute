// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

#include "Definitions.compute"

StructuredBuffer<Molecule> molecules;
RWStructuredBuffer<Molecule> moleculesOut;
int count;
float deltaTime;

float Rand(float2 seed) {
    return frac(sin(dot(seed, float2(12.9898,78.233))) * 43758.5453);
}

float SquareDistance(float3 a, float3 b)
{
    float3 diff = b - a;
    return dot(diff, diff); // Efficiently calculates the squared length of the difference
}

void CSMain_RAND(uint3 id)
{
    Molecule molecule = molecules[id.x];
    float xPos = Rand(float2(molecule.position.x, molecule.position.y));
    float yPos = Rand(float2(molecule.position.y, molecule.position.z));
    float zPos = Rand(float2(molecule.position.z, molecule.position.x));
    molecule.position = float3(xPos, yPos, zPos);
    moleculesOut[id.x] = molecule;
    
    float maximalDistance = -1;
    for (int i = 0; i < count; i++)
    {
        float distance = SquareDistance(molecules[i].position, molecule.position);
        if (distance > maximalDistance)
            maximalDistance = distance;
    }
    moleculesOut[id.x].maximalDistance = maximalDistance;
}

void CSMain_Move(uint3 id)
{
    Molecule molecule = molecules[id.x];
    
    float3 randDirection = float3(
        Rand(float2(molecule.position.x, molecule.position.y)),
        Rand(float2(molecule.position.y, molecule.position.z)),
        Rand(float2(molecule.position.z, molecule.position.x))
    );
    
    moleculesOut[id.x].position = molecule.position + molecule.velocity * deltaTime;
    moleculesOut[id.x].velocity = molecule.velocity + randDirection * 0.1f * deltaTime;
}

[numthreads(8,1,1)]
void CSMain (uint3 id : SV_DispatchThreadID)
{
    CSMain_Move(id);

}
