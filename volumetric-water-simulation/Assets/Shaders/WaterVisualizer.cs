using MarkusSecundus.Utils.Datastructs;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using VolumetricWaterSimulation;
using static UnityEngine.Rendering.HableCurve;

[RequireComponent(typeof(MoleculeSimulationController))]
public class WaterVisualizer : MonoBehaviour
{
    MoleculeSimulationController _simulation;
    [SerializeField] Mesh _waterDropletMesh;
    [SerializeField] Material _waterDropletMaterial;


    public void Reset()
    {
        OnDestroy();
        Start();
    }

    private void Start()
    {
        _simulation = GetComponent<MoleculeSimulationController>();

        if (_transparencySupplier) _transparencySupplier.onValueChanged.AddListener(SetTransparency);

        InitBuffers();
    }

    ComputeBuffer argsBuffer;

    void InitBuffers()
    {
        uint[] args = new uint[5] { 0, 0, 0, 0, 0 };
        // Arguments for drawing mesh.
        // 0 == number of triangle indices, 1 == population, others are only relevant if drawing submeshes.
        args[0] = (uint)_waterDropletMesh.GetIndexCount(0);
        args[1] = (uint)_simulation.MoleculesCount;
        args[2] = (uint)_waterDropletMesh.GetIndexStart(0);
        args[3] = (uint)_waterDropletMesh.GetBaseVertex(0);
        argsBuffer = new ComputeBuffer(1, args.Length * sizeof(uint), ComputeBufferType.IndirectArguments);
        argsBuffer.SetData(args);


        _waterDropletMaterial.SetBuffer("_Molecules", _simulation._moleculesBuffer);
        _waterDropletMaterial.SetInteger("_MoleculeCount", _simulation.MoleculesCount);
    }
    [SerializeField] Slider _transparencySupplier;

    [SerializeField] float _transparency = 1.0f;
    [SerializeField] float _scale = 1.0f;
    [SerializeField] float _adjustmentRadius = 1.0f;

    [SerializeField] float _frontFlattenFactor = 1.0f;
    [SerializeField] float _frontSkewFactor = 1.0f;
    [SerializeField] float _backFlattenFactor = 1.0f;
    [SerializeField] float _backSkewFactor = 1.0f;
    [SerializeField][Range(-1f,1f)] float _normalDifferentiator = 1.0f;
    [SerializeField][Range(-1f,1f)] float _DepthDiff = 1.0f;
    [SerializeField] float _AlphaFactorMax = 1.0f;
    [SerializeField] float _AlphaFactorInterpolator = 1.0f;
    [SerializeField] float _AlphaFactorWindowRadius = 1.0f;
    void Update()
    {
        _waterDropletMaterial.SetFloat("_Transparency", _transparency);
        _waterDropletMaterial.SetFloat("_Scale", _scale);
        _waterDropletMaterial.SetFloat("_AdjustmentRadius", _adjustmentRadius);

        _waterDropletMaterial.SetFloat("_FrontFlattenFactor", _frontFlattenFactor);
        _waterDropletMaterial.SetFloat("_FrontSkewFactor", _frontSkewFactor);
        _waterDropletMaterial.SetFloat("_BackFlattenFactor", _backFlattenFactor);
        _waterDropletMaterial.SetFloat("_BackSkewFactor", _backSkewFactor);
        _waterDropletMaterial.SetFloat("_NormalDifferentiator", _normalDifferentiator);
        _waterDropletMaterial.SetFloat("_DepthDiff", _DepthDiff);
        _waterDropletMaterial.SetFloat("_AlphaFactorMax", _AlphaFactorMax);
        _waterDropletMaterial.SetFloat("_AlphaFactorInterpolator", _AlphaFactorInterpolator);
        _waterDropletMaterial.SetFloat("_AlphaFactorWindowRadius", _AlphaFactorWindowRadius);

        _waterDropletMaterial.SetMatrix("_ParentTransform", Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one));

        //optimization
        _waterDropletMaterial.SetVector("_boundsSize", _simulation.bounds.size);
        _waterDropletMaterial.SetFloat("_smoothingRadius", _simulation.SmoothingRadius);

        //Debug.Log($"Buffers: {_waterDropletMaterial.HasBuffer("_moleculeIndices")}, {_waterDropletMaterial.HasBuffer("_moleculeCellIndices")}, {_waterDropletMaterial.HasBuffer("_cellOffsets")}, ", this);

        _waterDropletMaterial.SetBuffer("_moleculeIndices", _simulation._moleculeIndices);
        _waterDropletMaterial.SetBuffer("_moleculeCellIndices", _simulation._moleculeCellIndices);
        _waterDropletMaterial.SetBuffer("_cellOffsets", _simulation._cellOffsets);

        Graphics.DrawMeshInstancedIndirect(_waterDropletMesh, 0, _waterDropletMaterial, new Bounds(transform.position, Vector3.one * 999), argsBuffer);
    }

    public void SetTransparency(float value) => this._transparency = value;

    private void OnDestroy()
    {
        if (argsBuffer != null)
        {
            argsBuffer.Release();
        }
        argsBuffer = null;
    }
}
