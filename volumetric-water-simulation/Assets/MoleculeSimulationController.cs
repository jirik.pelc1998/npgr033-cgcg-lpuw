using MarkusSecundus.PhysicsSwordfight.Utils.Randomness;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.ParticleSystem;


// ANY CODE RELATED TO OPTIMIZATION WAS ADAPTED FROM THIS VIDEO by AJTech:
// https://www.youtube.com/watch?v=9M72KrGhYuE


namespace VolumetricWaterSimulation
{
    public sealed class MoleculeSimulationController : MonoBehaviour
    {        
        [Header("Compute Shader")]
        public ComputeShader computeShader;

        [Header("Molecule Related Constants")]
        public int MoleculesCount = 1000;
        public float MoleculeRadius = 0.1f;
        public float MoleculeMass = 0.1f;

        [Header("Collision Related Constant(s)")]
        public float CollisionDamping = -0.95f;

        [Header("SPH Related Constants")]
        public float Gravity = 9f;
        public float SmoothingRadius = 1.2f;
        public float PressureMultiplier = 100f;
        public float RestDensity = 4f;
        public float Viscosity = 0.5f;

        public Bounds bounds;
        public Molecule[] molecules;
        
        public int Seed;

        public ComputeBuffer _moleculesBuffer;

        private int externalForcesAndPredictPosKernel;
        private int densityKernel;
        private int pressureForceKernel;
        private int viscosityKernel;
        private int simulateStepKernel;

        // OPTIMIZATION

        public ComputeBuffer _moleculeIndices;
        public ComputeBuffer _moleculeCellIndices;
        public ComputeBuffer _cellOffsets;
        private int hashKernel;
        private int sortKernel;
        private int cellOffsetsKernel;

        // ------------

        public void Reset()
        {
            OnDestroy();
            Awake();
        }

        private void Awake()
        {
            SpawnMolecules();
            PrepareShaders();
        }

        private void OnDestroy()
        {
            if (_moleculesBuffer != null) _moleculesBuffer.Dispose();
            if (_moleculeIndices != null) _moleculeIndices.Dispose();
            if (_moleculeCellIndices != null) _moleculeCellIndices.Dispose();
            if (_cellOffsets != null) _cellOffsets.Dispose();
        }

        private void FixedUpdate()
        {
            computeShader.SetFloat("_moleculeMass", MoleculeMass);

            computeShader.SetVector("_boundsMaximum", bounds.max);
            computeShader.SetVector("_boundsMinimum", bounds.min);
            computeShader.SetVector("_boundsCenter", bounds.center);
            computeShader.SetMatrix("_boundsRotation", Matrix4x4.Rotate(transform.rotation));
            computeShader.SetFloat("_collisionDamping", CollisionDamping);

            computeShader.SetFloat("_fixedDeltaTime", Time.fixedDeltaTime);

            computeShader.SetFloat("_gravityAcceleration", Gravity);
            computeShader.SetFloat("_smoothingRadius", SmoothingRadius);
            computeShader.SetFloat("_pressureMultiplier", PressureMultiplier);
            computeShader.SetFloat("_restDensity", RestDensity);
            computeShader.SetFloat("_viscosity", Viscosity);

            computeShader.SetFloat("_smoothingRadius2", SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius3", SmoothingRadius * SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius4", SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius5", SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius6", SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius8", SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius);
            computeShader.SetFloat("_smoothingRadius9", SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius * SmoothingRadius);

            // OPTIMIZATION

            computeShader.Dispatch(hashKernel, MoleculesCount / 256, 1, 1);
            SortParticles();
            computeShader.Dispatch(cellOffsetsKernel, MoleculesCount / 256, 1, 1);

            // ------------

            computeShader.Dispatch(externalForcesAndPredictPosKernel, MoleculesCount / 256, 1, 1);
            computeShader.Dispatch(densityKernel, MoleculesCount / 256, 1, 1);
            computeShader.Dispatch(pressureForceKernel, MoleculesCount / 256, 1, 1);
            computeShader.Dispatch(viscosityKernel, MoleculesCount / 256, 1, 1);
            computeShader.Dispatch(simulateStepKernel, MoleculesCount / 256, 1, 1);

            _moleculesBuffer.GetData(molecules);
            //Debug.Log($"Density: <{molecules.Select(m=>m.density).Min()}, {molecules.Select(m => m.density).Max()}>", this);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.DrawWireCube(bounds.center + transform.position, bounds.size);

            if (false && molecules != null)
            {
                foreach (var molecule in molecules)
                {
                    Gizmos.DrawSphere(molecule.position, MoleculeRadius);
                }
            }
        }


        private void SpawnMolecules()
        {
            System.Random rnd = new System.Random(Seed);
            molecules = new Molecule[MoleculesCount];
            for (int i = 0; i < MoleculesCount; i++)
            {
                molecules[i] = new Molecule
                {
                    position = rnd.NextVector3(bounds),
                    velocity = Vector3.zero,
                    density = RestDensity
                };
            }
        }

        private void PrepareShaders()
        {
            computeShader.SetInt("_moleculesCount", MoleculesCount);
            computeShader.SetFloat("_moleculeRadius", MoleculeRadius);

            computeShader.SetVector("_boundsMaximum", bounds.max);
            computeShader.SetVector("_boundsMinimum", bounds.min);
            computeShader.SetMatrix("_boundsRotation", Matrix4x4.Rotate(transform.rotation));

            computeShader.SetFloat("_pi", Mathf.PI);
            computeShader.SetVector("_downVector", Vector3.down);

            externalForcesAndPredictPosKernel = computeShader.FindKernel("ComputeExternalForcesAndPredictPos");
            densityKernel = computeShader.FindKernel("ComputeDensity");
            pressureForceKernel = computeShader.FindKernel("ApplyPressureForce");
            viscosityKernel = computeShader.FindKernel("ApplyViscosity");
            simulateStepKernel = computeShader.FindKernel("SimulateStep");

            _moleculesBuffer = new ComputeBuffer(MoleculesCount, Molecule.Stride);
            _moleculesBuffer.SetData(molecules);
            Debug.Log($"Setting data to molecules buffer");

            computeShader.SetBuffer(externalForcesAndPredictPosKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(densityKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(pressureForceKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(viscosityKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(simulateStepKernel, "_molecules", _moleculesBuffer);

            // OPTIMIZATION

            hashKernel = computeShader.FindKernel("HashMolecules");
            sortKernel = computeShader.FindKernel("BitonicSort");
            cellOffsetsKernel = computeShader.FindKernel("ComputeCellOffsets");

            computeShader.SetBuffer(hashKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(sortKernel, "_molecules", _moleculesBuffer);
            computeShader.SetBuffer(cellOffsetsKernel, "_molecules", _moleculesBuffer);

            _moleculeIndices = new ComputeBuffer(MoleculesCount, sizeof(uint));
            _moleculeCellIndices = new ComputeBuffer(MoleculesCount, sizeof(uint));
            _cellOffsets = new ComputeBuffer(MoleculesCount, sizeof(uint));

            uint[] particleIndices = new uint[MoleculesCount];

            for (uint i = 0; i < particleIndices.Length; i++)
                particleIndices[i] = i;

            _moleculeIndices.SetData(particleIndices);

            computeShader.SetVector("_boundsSize", bounds.size);

            computeShader.SetBuffer(densityKernel, "_moleculeIndices", _moleculeIndices);
            computeShader.SetBuffer(pressureForceKernel, "_moleculeIndices", _moleculeIndices);
            computeShader.SetBuffer(viscosityKernel, "_moleculeIndices", _moleculeIndices);
            computeShader.SetBuffer(hashKernel, "_moleculeIndices", _moleculeIndices);
            computeShader.SetBuffer(sortKernel, "_moleculeIndices", _moleculeIndices);
            computeShader.SetBuffer(cellOffsetsKernel, "_moleculeIndices", _moleculeIndices);

            computeShader.SetBuffer(densityKernel, "_moleculeCellIndices", _moleculeCellIndices);
            computeShader.SetBuffer(pressureForceKernel, "_moleculeCellIndices", _moleculeCellIndices);
            computeShader.SetBuffer(viscosityKernel, "_moleculeCellIndices", _moleculeCellIndices);
            computeShader.SetBuffer(hashKernel, "_moleculeCellIndices", _moleculeCellIndices);
            computeShader.SetBuffer(sortKernel, "_moleculeCellIndices", _moleculeCellIndices);
            computeShader.SetBuffer(cellOffsetsKernel, "_moleculeCellIndices", _moleculeCellIndices);

            computeShader.SetBuffer(densityKernel, "_cellOffsets", _cellOffsets);
            computeShader.SetBuffer(pressureForceKernel, "_cellOffsets", _cellOffsets);
            computeShader.SetBuffer(viscosityKernel, "_cellOffsets", _cellOffsets);
            computeShader.SetBuffer(hashKernel, "_cellOffsets", _cellOffsets);
            computeShader.SetBuffer(sortKernel, "_cellOffsets", _cellOffsets);
            computeShader.SetBuffer(cellOffsetsKernel, "_cellOffsets", _cellOffsets);

        }

        private void SortParticles()
        {
            for (var dim = 2; dim <= MoleculesCount; dim <<= 1)
            {
                computeShader.SetInt("dim", dim);
                for (var block = dim >> 1; block > 0; block >>= 1)
                {
                    computeShader.SetInt("block", block);
                    computeShader.Dispatch(sortKernel, MoleculesCount / 256, 1, 1);
                }

            }
        }
    }
}