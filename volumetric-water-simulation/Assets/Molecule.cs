﻿using System.Runtime.InteropServices;
using UnityEngine;

namespace VolumetricWaterSimulation
{
    [System.Serializable]
    [StructLayout(LayoutKind.Sequential, Size = 40)]
    public struct Molecule
    {
        public Vector3 position; // 12
        public Vector3 positionPredicted; // 24
        public Vector3 velocity; // 36
        public float density; // 40

        public static int Stride => 40;
    }
}

