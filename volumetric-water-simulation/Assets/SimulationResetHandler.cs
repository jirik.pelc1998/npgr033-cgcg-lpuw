using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using VolumetricWaterSimulation;

public class SimulationResetHandler : MonoBehaviour
{
    [SerializeField] TMP_InputField _countSupplier;

    public void ResetTheSimulation()
    {
        int? count = null;
        if (int.TryParse(_countSupplier.text, out int cnt)) count = cnt ;

        foreach(var sim in GetComponents<MoleculeSimulationController>())
        {
            if (count is int c) sim.MoleculesCount = c;
            sim.Reset();
        }
        foreach (var vis in GetComponents<WaterVisualizer>())
            vis.Reset();
    }
}
