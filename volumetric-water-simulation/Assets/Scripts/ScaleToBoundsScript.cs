using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VolumetricWaterSimulation;

[ExecuteAlways]
public class ScaleToBoundsScript : MonoBehaviour
{
    public MoleculeSimulationController simulation; // Reference to the simulation controller

    private void Update()
    {
        if (simulation == null)
            return;
        // Get bounds from the simulation
        Bounds bounds = simulation.bounds;

        // Calculate the new scale based on the bounds extents
        Vector3 newScale = bounds.extents * 2;

        // Update the position and rotation of the cube
        transform.position = simulation.transform.position + bounds.center;
        transform.rotation = simulation.transform.rotation;

        // Apply the new scale to the cube
        transform.localScale = newScale;
    }
}
