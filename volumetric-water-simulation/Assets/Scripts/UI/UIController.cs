using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace VolumetricWaterSimulation.UI
{
    public class UIController : MonoBehaviour
    {
        [Header("Scale")]
        [SerializeField] private GameObject _scaleRoot;
        [SerializeField] private AxisLine _scaleX;
        [SerializeField] private AxisLine _scaleY;
        [SerializeField] private AxisLine _scaleZ;

        [Header("Rotation")]
        [SerializeField] private GameObject _rotationRoot;
        [SerializeField] private AxisLine _rotationX;
        [SerializeField] private AxisLine _rotationY;
        [SerializeField] private AxisLine _rotationZ;

        [Header("Animation axis")]
        [SerializeField] private GameObject _animationAxisRoot;
        [SerializeField] private AxisLine _animationAxisX;
        [SerializeField] private AxisLine _animationAxisY;
        [SerializeField] private AxisLine _animationAxisZ;

        [Header("Animation speed")]
        [SerializeField] private GameObject _animationSpeedRoot; 
        [SerializeField] private AxisLine _animationSpeed;

        [Header("Simulation")]
        [SerializeField] public MoleculeSimulationController _simulation;

        [Header("Configuration")]
        [SerializeField] private Vector3 _defaultRotationAxis = Vector3.forward;



        private IEnumerator _animationCoroutine;
        private Vector3 _currentAnimationAxis = Vector3.forward;

        void Awake()
        {
            Assert.IsNotNull(_simulation);

            Assert.IsNotNull(_scaleRoot);
            Assert.IsNotNull(_scaleX);
            Assert.IsNotNull(_scaleY);
            Assert.IsNotNull(_scaleZ);

            _scaleX._valueSlider.value = _simulation.bounds.extents.x;
            _scaleY._valueSlider.value = _simulation.bounds.extents.y;
            _scaleZ._valueSlider.value = _simulation.bounds.extents.z;

            Assert.IsNotNull(_rotationRoot);
            Assert.IsNotNull(_rotationX);
            Assert.IsNotNull(_rotationY);
            Assert.IsNotNull(_rotationZ);

            _rotationX._valueSlider.value = _simulation.transform.eulerAngles.x;
            _rotationY._valueSlider.value = _simulation.transform.eulerAngles.y;
            _rotationZ._valueSlider.value = _simulation.transform.eulerAngles.z;

            Assert.IsNotNull(_animationAxisRoot);
            Assert.IsNotNull(_animationAxisX);
            Assert.IsNotNull(_animationAxisY);
            Assert.IsNotNull(_animationAxisZ);

            _currentAnimationAxis = _defaultRotationAxis.normalized;

            _animationAxisX._valueSlider.value = _currentAnimationAxis.x;
            _animationAxisY._valueSlider.value = _currentAnimationAxis.y;
            _animationAxisZ._valueSlider.value = _currentAnimationAxis.z;


            _scaleX.ValueChanged.AddListener(OnScaleChanged);
            _scaleY.ValueChanged.AddListener(OnScaleChanged);
            _scaleZ.ValueChanged.AddListener(OnScaleChanged);

            _rotationX.ValueChanged.AddListener(OnRotationChanged);
            _rotationY.ValueChanged.AddListener(OnRotationChanged);
            _rotationZ.ValueChanged.AddListener(OnRotationChanged);
            _animationAxisX.ValueChanged.AddListener(OnAnimationAxisChanged);
            _animationAxisY.ValueChanged.AddListener(OnAnimationAxisChanged);
            _animationAxisZ.ValueChanged.AddListener(OnAnimationAxisChanged);
        }

        void OnScaleChanged(float _)
        {
            _simulation.bounds.extents = new Vector3(
                _scaleX.Value,
                _scaleY.Value,
                _scaleZ.Value
            );
        }

        void OnRotationChanged(float _)
        {
            _simulation.transform.localEulerAngles = new Vector3(
                _rotationX.Value,
                _rotationY.Value,
                _rotationZ.Value
            );
        }

        void OnAnimationAxisChanged(float _)
        {
            _currentAnimationAxis = new Vector3(
                _animationAxisX.Value,
                _animationAxisY.Value,
                _animationAxisZ.Value
            ).normalized;
        }

        public void SetManualControll()
        {
            _rotationRoot.SetActive(true);
            _scaleRoot.SetActive(true);
            _animationAxisRoot.SetActive(false);
            _animationSpeedRoot.SetActive(false);

            if(_animationCoroutine != null)
            {
                StopCoroutine(_animationCoroutine);
                _animationCoroutine = null;
            }
        }

        public void SetAutomaticControll()
        {
            _rotationRoot.SetActive(false);
            _scaleRoot.SetActive(false);
            _animationAxisRoot.SetActive(true);
            _animationSpeedRoot.SetActive(true);

            _animationCoroutine = AnimationCoroutine();
            StartCoroutine(_animationCoroutine);
        }

        public void SetHide()
        {
            _rotationRoot.SetActive(false);
            _scaleRoot.SetActive(false);
            _animationAxisRoot.SetActive(false);
            _animationSpeedRoot.SetActive(false);
        }

        private IEnumerator AnimationCoroutine()
        {
            while (true)
            {
                _simulation.transform.Rotate(_currentAnimationAxis, Time.deltaTime * _animationSpeed.Value);
                yield return null;
            }
        }
    }
}
