using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.Events;
using UnityEngine.UI;

namespace VolumetricWaterSimulation.UI
{
    public class AxisLine : MonoBehaviour
    {
        [SerializeField] internal Slider _valueSlider;
        [SerializeField] internal TMPro.TMP_Text _valueText;
        [SerializeField] private string _format;

        public float Value => _valueSlider.value;
        public UnityEvent<float> ValueChanged;

        // Start is called before the first frame update
        void Awake()
        {
            Assert.IsNotNull(_valueSlider);
            Assert.IsNotNull(_valueText);

            _valueSlider.onValueChanged.AddListener(OnValueChanged);
            _valueText.text = _valueSlider.value.ToString(_format);
        }

        void OnValueChanged(float newValue)
        {
            _valueText.text = newValue.ToString(_format);
            ValueChanged.Invoke(newValue);
        }

        public void SetValue(float newValue)
        {
            _valueSlider.value = newValue;
            _valueText.text = newValue.ToString(_format);
        }
    }
}
