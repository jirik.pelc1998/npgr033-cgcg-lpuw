using UnityEngine;
using UnityEngine.Serialization;

/// <summary>
/// A script to rotate the camera around the zero point at a variable distance based on middle mouse click and drag action, and to adjust the distance using the scroll wheel.
/// </summary>
public class CameraRotationScript : MonoBehaviour
{
    public Transform Camera;
    public Vector3 OrbitCenter;
    public float HorizontalSpeed;
    public float VerticalSpeed;
    public Vector2 VerticalRotationLimits;
    public Vector2 ZoomLimits;
    public float ZoomSpeed;

    public float _CurrentDistance;

    private Vector2 _CurrentCameraSpeed;



    /// <summary>
    /// Start is called before the first frame update.
    /// </summary>
    void Start()
    {
        // Initialize the current distance
        _CurrentDistance = Mathf.Clamp(_CurrentDistance, ZoomLimits.x, ZoomLimits.y);
    }

    private void LateUpdate()
    {
        AdjustDistance();
        AdjustRotation();
    }

    /// <summary>
    /// Adjusts the distance of the camera from the zero point based on the scroll wheel action.
    /// </summary>
    private void AdjustRotation()
    {
        if (Input.GetKey(KeyCode.Mouse2)) // Middle mouse button clicked
        {
            var vertical = Input.GetAxis("Mouse Y") * VerticalSpeed * Time.deltaTime;
            var horizontal = Input.GetAxis("Mouse X") * HorizontalSpeed * Time.deltaTime;

            transform.Rotate(Vector3.right, vertical);
            transform.Rotate(Vector3.up, horizontal, Space.World);
        }
    }

    /// <summary>
    /// Adjusts the distance of the camera from the zero point based on the scroll wheel action.
    /// </summary>
    private void AdjustDistance()
    {
        float scroll = Input.mouseScrollDelta.y;
        if (scroll != 0.0f)
        {
            _CurrentDistance = Mathf.Clamp(_CurrentDistance - scroll * ZoomSpeed, ZoomLimits.x, ZoomLimits.y);
            Camera.transform.localPosition = Vector3.back * _CurrentDistance;
        }
    }
}