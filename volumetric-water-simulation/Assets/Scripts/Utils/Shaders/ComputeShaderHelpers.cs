﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace MarkusSecundus.Utils.Shaders
{
    public static class ComputeShaderHelpers
    {

        static ThreadLocal<float[]> _vector2 = new ThreadLocal<float[]>(()=>new float[2]);
        public static void SetVector2(this ComputeShader self, string name, Vector2 v)
        {
            var vec = _vector2.Value;
            (vec[0], vec[1]) = (v.x, v.y);
            self.SetFloats(name, vec);
        }
        static ThreadLocal<float[]> _vector3 = new ThreadLocal<float[]>(()=>new float[3]);
        public static void SetVector3(this ComputeShader self, string name, Vector3 v)
        {
            var vec = _vector3.Value;
            (vec[0], vec[1], vec[2]) = (v.x, v.y, v.z);
            self.SetFloats(name, vec);
        }
        static ThreadLocal<int[]> _vector2int = new ThreadLocal<int[]>(()=>new int[2]);
        public static void SetVector2Int(this ComputeShader self, string name, Vector2Int v)
        {
            var vec = _vector2int.Value;
            (vec[0], vec[1]) = (v.x, v.y);
            self.SetInts(name, vec);
        }
    }
}
