﻿using Assets.Scripts.Utils.Filesystem;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Utils.Editor
{
    public static class EditorUtils
    {
        public static void InstantiateBlobIntoCurrentDirectory(byte[] templateBlob, string requestedFileName)
        {
            if (Selection.assetGUIDs.Length < 1)
                throw new System.InvalidOperationException("Can only create new file if a folder is active!");
            string folderGUID = Selection.assetGUIDs[0];
            string projectFolderPath = AssetDatabase.GUIDToAssetPath(folderGUID);
            string folderDirectory = Path.GetFullPath(projectFolderPath);

            File.WriteAllBytes(FileUtils.GetUnoccupiedFilePathIncremental(Path.Combine(folderDirectory, requestedFileName)), templateBlob);

            AssetDatabase.Refresh();
        }
    }
}
