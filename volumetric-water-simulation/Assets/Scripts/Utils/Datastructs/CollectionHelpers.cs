using MarkusSecundus.PhysicsSwordfight.Utils.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Unity.Properties;

namespace MarkusSecundus.Utils.Datastructs
{
    /// <summary>
    /// Static class containing convenience extensions methods for standard collections
    /// </summary>
    public static class CollectionHelpers
    {
        /// <summary>
        /// Yield given number of results obtained from a given supplier
        /// </summary>
        /// <typeparam name="T">Type of the element</typeparam>
        /// <param name="supplier">Supplier for the iteration elements</param>
        /// <param name="count">How many elements</param>
        /// <returns>Generator that yields elements obtained from <paramref name="supplier"/></returns>
        public static IEnumerable<T> Repeat<T>(this System.Func<T> supplier, int count)
        {
            while (--count >= 0)
                yield return supplier();
        }

        /// <summary>
        /// Get last element of a list
        /// </summary>
        /// <typeparam name="T">Type of list's elements</typeparam>
        /// <param name="self">List to be peeked</param>
        /// <returns>Last element of the list</returns>
        public static T Peek<T>(this IReadOnlyList<T> self) => self[self.Count - 1];
        /// <summary>
        /// Get last element of a list if it has any
        /// </summary>
        /// <typeparam name="T">Type of list's elements</typeparam>
        /// <param name="self">List to be peeked</param>
        /// <param name="ret">Last element of the list or <c>default</c></param>
        /// <returns><c>true</c> if the list is non-empty</returns>
        public static bool TryPeek<T>(this IReadOnlyList<T> self, out T ret)
        {
            if (self.IsNullOrEmpty())
            {
                ret = default;
                return false;
            }
            else
            {
                ret = self.Peek();
                return true;
            }
        }
        /// <summary>
        /// Generator that lazily iterates through given stream given number of times
        /// </summary>
        /// <typeparam name="T">Type of stream's elements</typeparam>
        /// <param name="self">Stream to be iterated multiple times</param>
        /// <param name="repeatCount">How many times to iterate through the stream</param>
        /// <returns>Generator that lazily iterates through given stream given number of times</returns>
        public static IEnumerable<T> RepeatList<T>(this IEnumerable<T> self, int repeatCount)
        {
            while (--repeatCount >= 0)
                foreach (var i in self) yield return i;
        }

        /// <summary>
        /// Concatenates all elements into a string
        /// </summary>
        /// <typeparam name="T">Type of the elements</typeparam>
        /// <param name="self">Stream of elements to concatenate into a string</param>
        /// <param name="separator">Separator to be inserted between element's string representations</param>
        /// <returns>String concatenation of all provided elements</returns>
        public static string MakeString<T>(this IEnumerable<T> self, string separator = ", ", string ifNull = "<nil>", string ifEmpty="")
        {
            if (self.IsNil()) return ifNull;
            using var it = self.GetEnumerator();

            if (!it.MoveNext()) return ifEmpty;
            var ret = new StringBuilder().Append(it.Current.ToString());
            while (it.MoveNext()) ret = ret.Append(separator).Append(it.Current.ToString());

            return ret.ToString();
        }

        static class StructuredToStringHelper<T>
        {
            static Func<T, string>[] printoutGetters;
            static StructuredToStringHelper()
            {
                if (typeof(T).IsPrimitive) return;

                UnityEngine.Debug.Log($"Preparing type {typeof(T).Name}...");
                var printoutGetters = new List<Func<T, string>>();
                foreach(var p in typeof(T).GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public))
                {
                    UnityEngine.Debug.Log($"{typeof(T).Name}::{p.Name}");
                    var getterMethod = Delegate.CreateDelegate(typeof(Func<,>).MakeGenericType(typeof(T), p.PropertyType), p.GetGetMethod());
                    var arg = Expression.Parameter(typeof(T), "_instance");

                    var stringGetterExpression = Expression.Call(typeof(StructuredToStringHelper<>).MakeGenericType(p.PropertyType).GetMethod(nameof(ToString), System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public)
                        , Expression.Invoke(Expression.Constant(getterMethod), arg));
                    var lambda = Expression.Lambda<Func<T, string>>(stringGetterExpression);
                    var builtLambda = lambda.Compile();
                    Func<T, string> valueGetter = t => $"{p.PropertyType}: '{builtLambda(t)}'";
                }
                StructuredToStringHelper<T>.printoutGetters = printoutGetters.ToArray();
            }
            const string Separator = ", ";
            public static string ToString(T self)
            {
                if (self == null) return "<nil>";
                if (typeof(T).IsPrimitive) return self.ToString();

                var bld = new StringBuilder().Append($"{typeof(T).Name}").Append("{");

                if(printoutGetters.Length > 0)
                {
                    bld.Append(printoutGetters[0](self));
                    for (int t = 1; t < printoutGetters.Length; ++t) bld.Append(Separator).Append(printoutGetters[t](self));
                }

                bld.Append("}");
                
                return bld.ToString();
            }
        }

        public static string ToStringStructured<T>(this T self) => StructuredToStringHelper<T>.ToString(self);

        /// <summary>
        /// If the collection is null or empty
        /// </summary>
        /// <typeparam name="T">Type of elements</typeparam>
        /// <param name="self">Collection to be checked for emptiness</param>
        /// <returns><c>true</c> iff the collection is null or empty</returns>
        public static bool IsNullOrEmpty<T>(this IReadOnlyCollection<T> self) => self.IsNil() || self.Count <= 0;

        /// <summary>
        /// Gets smallest value in a stream, using provided selector for comparisons.
        /// </summary>
        /// <typeparam name="T">Type of the elements</typeparam>
        /// <typeparam name="TComp">Type of the elements used for comparison</typeparam>
        /// <param name="self">Stream to be searched through</param>
        /// <param name="selector">Function for obtaining comparable representative for each element</param>
        /// <returns>Value whose representative was the smallest</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">If the provided stream is empty</exception>
        public static T Minimal<T, TComp>(this IEnumerable<T> self, System.Func<T, TComp> selector) where TComp : System.IComparable<TComp>
        {
            using var it = self.GetEnumerator();
            if (!it.MoveNext()) throw new System.ArgumentOutOfRangeException("Empty collection was provided!");

            var ret = it.Current;
            var min = selector(ret);

            while (it.MoveNext())
            {
                var cmp = selector(it.Current);
                if (cmp.CompareTo(min) < 0)
                {
                    min = cmp;
                    ret = it.Current;
                }
            }

            return ret;
        }
        public static Dictionary<TKey, TValue> ToDictionary<TKey, TValue>(this IEnumerable<KeyValuePair<TKey, TValue>> self) => new Dictionary<TKey, TValue>(self);

        public static T[] Concat<T>(this T[] self, T[] toConcat)
        {
            if (self.IsNullOrEmpty()) return toConcat ?? System.Array.Empty<T>();
            if (toConcat.IsNullOrEmpty()) return self ?? System.Array.Empty<T>();
            T[] ret = new T[self.Length + toConcat.Length];
            System.Array.Copy(self, ret, self.Length);
            System.Array.Copy(toConcat, 0, ret, self.Length, toConcat.Length);
            return ret;
        }

        public struct IndexedEnumerator<TCollection, TItem> : IEnumerator<TItem>, IEnumerator where TCollection : IReadOnlyList<TItem>
        {
            TCollection _base;
            int _currentIndex;
            public IndexedEnumerator(TCollection baseCollection) => (_base, _currentIndex) = (baseCollection, -1);
            public TItem Current { get
                {
                    if (_currentIndex < 0) throw new System.InvalidOperationException($"Reading Current before first calling MoveNext()");
                    return _base[_currentIndex];
                } }

            object IEnumerator.Current => Current;

            public void Dispose() { }

            public bool MoveNext() => (++_currentIndex) < _base.Count;

            public void Reset() => _currentIndex = -1;
        }

        public struct EnumeratorToEnumerable<T> : IEnumerable<T>, IEnumerable
        {
            IEnumerator<T> _base;
            public EnumeratorToEnumerable(IEnumerator<T> enumerator) => _base = enumerator;

            public EnumeratorNoDispose GetEnumerator() => new EnumeratorNoDispose(_base);
            IEnumerator<T> IEnumerable<T>.GetEnumerator() => GetEnumerator();

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public struct EnumeratorNoDispose : IEnumerator<T>
            {
                IEnumerator<T> _base;
                public EnumeratorNoDispose(IEnumerator<T> enumerator) => _base = enumerator;
                public T Current => _base.Current;

                object IEnumerator.Current => Current;

                public void Dispose() { }

                public bool MoveNext() => _base.MoveNext();

                public void Reset() => _base.Reset();
            }
        }
        public static EnumeratorToEnumerable<T> IterateNoDispose<T>(this IEnumerator<T> self)=> new EnumeratorToEnumerable<T>(self); 
    }
}
