using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VolumetricWaterSimulation;
using static Unity.VisualScripting.Member;
using static UnityEngine.ParticleSystem;

public class FluidRayMarching : MonoBehaviour
{
    public ComputeShader raymarching;
    public Camera cam;
    public RawImage _img;

    List<ComputeBuffer> buffersToDispose = new List<ComputeBuffer>();

    MoleculeSimulationController _simulation;

    RenderTexture target;

    [Header("Params")]
    public float viewRadius;
    public float blendStrength;
    public Color waterColor;

    public Color ambientLight;

    public Light lightSource;

    private void Awake()
    {
        _simulation = GetComponent<MoleculeSimulationController>();
    }

    void InitRenderTexture()
    {
        if (target == null || target.width != cam.pixelWidth || target.height != cam.pixelHeight)
        {
            if (target != null)
            {
                target.Release();
            }

            cam.depthTextureMode = DepthTextureMode.Depth;

            target = new RenderTexture(cam.pixelWidth, cam.pixelHeight, 0, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.Linear);
            target.enableRandomWrite = true;
            target.Create();
        }
        _img.texture = target;
    }


    private bool render = false;

    public ComputeBuffer _particlesBuffer;

    private void SpawnParticlesInBox()
    {
        _particlesBuffer = new ComputeBuffer(1, 44);
        _particlesBuffer.SetData(new Particle[] {
        new Particle {
        position = new Vector3(0,0,0)
       }});

    }

    public void Begin()
    {
        Debug.Log($"Raymarch BEGIN", this);
        // SpawnParticlesInBox();
        InitRenderTexture();
        raymarching.SetBuffer(0, "particles", _simulation._moleculesBuffer);
        raymarching.SetInt("numParticles", _simulation.MoleculesCount);
        raymarching.SetFloat("particleRadius", viewRadius);
        raymarching.SetFloat("blendStrength", blendStrength);
        raymarching.SetVector("waterColor", waterColor);
        raymarching.SetVector("_AmbientLight", ambientLight);
        raymarching.SetTextureFromGlobal(0, "_DepthTexture", "_CameraDepthTexture");
        render = true;
    }



    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Debug.Log("WaterRaymarcher::OnRenderImage() running", this);

        // InitRenderTexture();

        if (!render)
        {
            Begin();
        }

        if (render)
        {

            Debug.Log($"Raymarch render", this);
            raymarching.SetVector("_Light", lightSource.transform.forward);

            raymarching.SetTexture(0, "Source", source);
            raymarching.SetTexture(0, "Destination", target);
            raymarching.SetVector("_CameraPos", cam.transform.position);
            raymarching.SetMatrix("_CameraToWorld", cam.cameraToWorldMatrix);
            raymarching.SetMatrix("_CameraInverseProjection", cam.projectionMatrix.inverse);

            int threadGroupsX = Mathf.CeilToInt(cam.pixelWidth / 8.0f);
            int threadGroupsY = Mathf.CeilToInt(cam.pixelHeight / 8.0f);
            raymarching.Dispatch(0, threadGroupsX, threadGroupsY, 1);

            Graphics.Blit(target, destination);
        }
    }

    private void Update()
    {
        Debug.Log("WaterRaymarcher::Update() running", this);

        // InitRenderTexture();

        if (!render)
        {
            Begin();
        }

        if (render)
        {

            Debug.Log($"Raymarch render", this);
            raymarching.SetVector("_Light", lightSource.transform.forward);

            raymarching.SetTexture(0, "Source", target);
            raymarching.SetTexture(0, "Destination", target);
            raymarching.SetVector("_CameraPos", cam.transform.position);
            raymarching.SetMatrix("_CameraToWorld", cam.cameraToWorldMatrix);
            raymarching.SetMatrix("_CameraInverseProjection", cam.projectionMatrix.inverse);

            int threadGroupsX = Mathf.CeilToInt(cam.pixelWidth / 8.0f);
            int threadGroupsY = Mathf.CeilToInt(cam.pixelHeight / 8.0f);
            raymarching.Dispatch(0, threadGroupsX, threadGroupsY, 1);
        }
    }

}
